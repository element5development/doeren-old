<?php /*
TEMPLATE FOR SINGLE POST (VIEWPOINT)
*/ ?>

<!--URL for featured image-->
<?php 
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); 
	$featuredimg = $src[0];
?>



<?php get_header(); ?>

<main class="full-width full-page-container">

	<?php $primary_cat_id = get_post_meta(get_the_ID(),'_yoast_wpseo_primary_category',true); ?>
	<?php if ( get_field('category_header_image', 'term_'.$primary_cat_id) ) : ?>
		<div class="page-header custom-header max-width">
				<?php if ( get_field('category_title', 'term_'.$primary_cat_id) ) : ?>
					<div class="page-header-contents" style="background: transparent;">
						<h1><?php the_field('category_title', 'term_'.$primary_cat_id); ?></h1>
					</div>
				<?php endif; ?>
			<img src="<?php the_field('category_header_image', 'term_'.$primary_cat_id) ?>" alt="" />
		</div>
	<?php else : ?>
		<div class="page-header max-width" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/img/blog-bg.png);">
			<div class="page-header-contents" style="background: transparent;">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/viewpoint-logo.png" />
			</div>
		</div>
	<?php endif; ?>

	<div class="back-to-parent max-width">
		<a class="back-page" href="/viewpoint/">Back to All Posts</a>
		<?php $primary_cat_id = get_post_meta(get_the_ID(),'_yoast_wpseo_primary_category',true); ?>
		<?php if( $primary_cat_id ) : ?>
			<br><br><a class="back-page" href="<?php echo get_category_link($primary_cat_id) ?>">Back to <?php echo get_cat_name($primary_cat_id); ?> Posts</a>
		<?php endif; ?>
	</div>

	<div id="page-contents-container" class="max-width">
		<aside id="double-left-sidebar" class="widget-area-container">
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('two-sidebar-left')) : else : ?>
				<p><strong>Widget Ready</strong></p>  
			<?php endif; ?>  
		</aside>

		<section id="double-sidebar-contents">
			<div id="double-sidebar-page-contents" class="viewpoint-post-content">

				<div class="post-feed-date">
					<span class="month"><?php the_time('M') ?></span>	
					<span class="day"><?php the_time('j') ?></span>
					<span class="year"><?php the_time('Y') ?></span>
				</div>

				<h1 class="post-title"><?php the_title(); ?></h1>




				<?php if ( has_post_thumbnail() ) : ?>
					<div class="post-feed-image" style="background-image: url(<?php echo $featuredimg; ?>); background-size: cover; background-position: center center; width: 100%; height: 325px;" >
					</div>
				<?php endif; ?>

				<div class="post-content">
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>

				<div class="post-feed-cat">
					<h3>Categories</h3>
					<?php foreach((get_the_category()) as $category) {
						    echo '<a href="' . get_category_link( $category->term_id ) . '" class="' . sprintf( $category->slug ) . '" ' . '>' . $category->name.'</a> ';
					} ?>
				</div>
				<div class="widget">
					<p id="legal-text">This publication is distributed for informational purposes only, with the understanding that Doeren Mayhew is not rendering legal, accounting, or other professional opinions on specific facts for matters, and, accordingly, assumes no liability whatsoever in connection with its use. Should the reader have any questions regarding any of the news articles, it is recommended that a Doeren Mayhew representative be contacted.</p>
				</div>


			</div>
		</section>

		<aside id="double-right-sidebar" class="widget-area-container">
			<div class="widget categories-widget">
				<h2 class="right-sidebar-title">Categories</h2>
				<ul>
					<li class="top-level has-child">Services
						<ul class="child-categories">
							<?php $child_categories=get_categories(array( 'parent' => 24 )); ?>
							<?php foreach ( $child_categories as $child ) : ?>
								<li><a href="<?php echo get_category_link($child->term_id) ?>"><?php echo $child->cat_name; ?></a></li>
							<?php endforeach; ?>
						</ul>
					</li>
					<li class="top-level has-child">Specializations
						<ul class="child-categories">
							<?php $child_categories=get_categories(array( 'parent' => 23 )); ?>
							<?php foreach ( $child_categories as $child ) : ?>
								<li><a href="<?php echo get_category_link($child->term_id) ?>"><?php echo $child->cat_name; ?></a></li>
								<?php if ( $child->term_id == 40 ) : ?>
									<li><a href="<?php echo get_category_link(701) ?>"><?php echo get_cat_name(701); ?></a></li>
								<?php endif; ?>
							<?php endforeach; ?>
						</ul>
					</li>
					<li class="top-level"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">View All</a></li>
				</ul>
			</div>
			<?php $primary_cat_id = get_post_meta(get_the_ID(),'_yoast_wpseo_primary_category',true); ?>
			<?php if( $primary_cat_id == 701 ) : ?>
				<div class="widget widget_simpleimage">
					<a href="https://doeren.com/regulatory-compliance-viewpoint-subscription/"><img src="https://doeren.com/wp-content/uploads/2019/07/fig-compliance-viewpoint-sidebar-002.jpg" class="attachment-full size-full" alt="fig-compliance-viewpoint-sidebar (002)" srcset="https://doeren.com/wp-content/uploads/2019/07/fig-compliance-viewpoint-sidebar-002.jpg 470w, https://doeren.com/wp-content/uploads/2019/07/fig-compliance-viewpoint-sidebar-002-300x249.jpg 300w" sizes="(max-width: 470px) 85vw, 470px" width="470" height="390"></a>
					<p><a target="" href="https://doeren.com/regulatory-compliance-viewpoint-subscription/" class="dark-button">Subscribe Now</a></p>
				</div>
			<?php else : ?>
				<div id="text-24" class="widget widget_text">
					<h2 class="two-sidebar-right-title">Stay in the Know</h2>			
					<div class="textwidget">
						<p>Join our newsletter</p>
						<a target="" href="https://doeren.com/subscribe/" class="dark-button" rel="noopener noreferrer">Subscribe</a>
					</div>
				</div>
			<?php endif; ?>
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('two-sidebar-right')) : else : ?>
			<?php endif; ?>  
		</aside>

		<div style="clear: both"></div>
	</div>

</main>

<?php get_footer(); ?>