<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES & CATEGORY FOR CUSTOM POSTS TYPE "EVENTS"
*/ ?>

<?php get_header(); ?>

<?php
	$service = $_GET["service"];
	$specialization = $_GET["specialization"];
	$state = $_GET["state"];
	$format = $_GET["format"];

	//dental cpa both service and specialization
	if ( $specialization  == 'dental-cpa' ) {
		$service == 'dental-cpa';
		$specialization == 'none';
	}

	if ( $format == 'all' ) { unset($format); }
	if ( $service == 'none' ) { unset($service);  }
	if ( $specialization  == 'none' ) { unset($specialization);  }
	if ( $state  == 'none' ) { unset($state );  }


	if ( $service == NULL and $specialization == NULL and $state == NULL and $format == NULL) {
		/*NO VARIABLES PASSED*/
		$nothingpassed = "Nothing Passed";
		$args = [ 
			'post_type' => 'event', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
	    ];
	} elseif ( $specialization == NULL and $service == NULL and $state == NULL ) {
	    /*FORMAT FILTER*/
		$args = [ 
			'post_type' => 'event', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'meta_key' => 'format',
			'meta_value' => $format,
	    ];
	} elseif ( $specialization == NULL and $state == NULL ) {
	    /*ONLY SERVICE PASSED*/
		$args = [ 
			'post_type' => 'event', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'tax_query'=>array(
			    array(
			        'taxonomy' => 'event-category',
			        'field' => 'slug',
			        'terms' => $service,
			      )
			),
	    ];
	} elseif ( $service == NULL AND $state == NULL  ) {
	    /*ONLY SPECILIZATION PASSED */
		$args = [ 
			'post_type' => 'event', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'tax_query'=>array(
			    array(
			        'taxonomy' => 'event-category',
			        'field' => 'slug',
			        'terms' => $specialization,
			    )
			),
	    ];
	} elseif ( $service == NULL AND $specialization == NULL  ) {
	    /*ONLY STATE PASSED */
		$args = [ 
			'post_type' => 'event', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'meta_key' => 'state',
			'meta_value' => $state,
	    ];
	} elseif ( $specialization == NULL  ) {
	    /*ONLY STATE & SERVICE PASSED */
		$args = [ 
			'post_type' => 'event', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'meta_key' => 'state',
			'meta_value' => $state,
			'tax_query'=>array(
			    array(
			        'taxonomy' => 'event-category',
			        'field' => 'slug',
			        'terms' => $specialization,
			    )
			),
	    ];
	} elseif ( $service == NULL  ) {
	    /*ONLY STATE & SPECIALIZATION PASSED */
		$args = [ 
			'post_type' => 'event', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'meta_key' => 'state',
			'meta_value' => $state,
			'tax_query'=>array(
			    array(
			        'taxonomy' => 'event-category',
			        'field' => 'slug',
			        'terms' => $specialization,
			    )
			),
	    ];
	} elseif ( $state == NULL  ) {
	    /*ONLY SERVICE & SPECIALIZATION PASSED */
		$args = [ 
			'post_type' => 'event', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'tax_query'=>array(
			    array(
			        'taxonomy' => 'event-category',
			        'field' => 'slug',
			        'terms' => $specialization,
			    )
			),
	    ];
	} else {
		/*SERVICE & SPECALIZATION PASSED*/
		$categories = array( $service, $specialization );
		$args = [ 
			'post_type' => 'event', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'meta_key' => 'state',
			'meta_value' => $state,
			'tax_query'=>array( 
				'relation' => 'AND',
				array(
			        'taxonomy' => 'event-category',
			        'field' => 'slug',
			        'terms' => $specialization,
			    ),
			    array(
			        'taxonomy' => 'event-category',
			        'field' => 'slug',
			        'terms' => $service,
			    )

			),
	    ];
	}
?>



<main class="full-width">

	<div class="page-header max-width">
		<?php echo do_shortcode('[rev_slider alias="events-page"]'); ?>
	</div>

	<div id="page-contents-container" class="max-width archive-container">
		<div class="filter">
			<div class="filter-search events">
				<form id="category-filter" method="get" action="/events/event/">
				    <h2>Search for an Event</h2>
				    <select name="service">
				    	<option value="none">Service</option>
				    	<option value="accounting-audit-assurance">Accounting, Audit and Assurance</option>
				    	<option value="business-advisory">Business Advisory</option>
				    	<option value="dental-cpa">Dental CPA</option>
				    	<option value="employee-benefit-plans">Employee Benefit Plans</option>
				    	<option value="insurance">Insurance</option>
				    	<option value="international-advisory">International Advisory</option>
				    	<option value="it-assurance-security">IT Assurance Security</option>
				    	<option value="mergers-acquisitions">Mergers Acquisitions</option>
				    	<option value="payroll">Payroll</option>
				    	<option value="small-business-advisory">Small Business Advisory</option>
				    	<option value="strategic-advisory">Strategic Advisory</option>
				    	<option value="tax">Tax Compliance and Planning</option>
				    	<option value="tax-incentives">Tax Incentives</option>
				    	<option value="valuation-litigation-support">Valuation Litigation Support</option>
				    	<option value="wealth-management">Wealth Management</option>
				    </select>
				    <select name="specialization">
				    	<option value="none">Specialization</option>
				    	<option value="automotive-dealerships">Automotive Dealerships</option>
				    	<option value="closely-held-businesses">Closely Held Businesses</option>
				    	<option value="construction">Construction</option>
				    	<option value="dental-cpa">Dental CPA</option>
				    	<option value="energy">Energy</option>
				    	<option value="financial-institutions">Financial Institutions</option>
				    	<option value="government-non-profit">Government & Non-Profit</option>
				    	<option value="healthcare">Healthcare</option>
				    	<option value="international-companies">International Companies</option>
				    	<option value="public-companies">Public Companies</option>
				    	<option value="real-estate">Real Estate</option>
				    	<option value="retail-restaurant">Retail Restaurant</option>
				    	<option value="service-providers">Service Providers</option>
				    	<option value="small-businesses">Small Businesses</option>
				    	<option value="technologies">Technologies</option>
				    	<option value="wholesale-distribution">Wholesale & Distribution</option>
				    </select>
				    <select name="state">
				    	<option value="none">State</option>
				    	<option value="AL">AL</option>
				    	<option value="AK">AK</option>
				    	<option value="AZ">AZ</option>
				    	<option value="AR">AR</option>
				    	<option value="CA">CA</option>
				    	<option value="CO">CO</option>
				    	<option value="CT">CT</option>
				    	<option value="DE">DE</option>
				    	<option value="DC">DC</option>
				    	<option value="FL">FL</option>
				    	<option value="GA">GA</option>
				    	<option value="HI">HI</option>
				    	<option value="ID">ID</option>
				    	<option value="IL">IL</option>
				    	<option value="IN">IN</option>
				    	<option value="IA">IA</option>
				    	<option value="KS">KS</option>
				    	<option value="KY">KY</option>
				    	<option value="LA">LA</option>
				    	<option value="ME">ME</option>
				    	<option value="MD">MD</option>
				    	<option value="MA">MA</option>
				    	<option value="MI">MI</option>
				    	<option value="MN">MN</option>
				    	<option value="MS">MS</option>
				    	<option value="MO">MO</option>
				    	<option value="MT">MT</option>
				    	<option value="NE">NE</option>
				    	<option value="NV">NV</option>
				    	<option value="NH">NH</option>
				    	<option value="NJ">NJ</option>
				    	<option value="NM">NM</option>
				    	<option value="NY">NY</option>
				    	<option value="NC">NC</option>
				    	<option value="ND">ND</option>
				    	<option value="OH">OH</option>
				    	<option value="OK">OK</option>
				    	<option value="OR">OR</option>
				    	<option value="PA">PA</option>
				    	<option value="RI">RI</option>
				    	<option value="SC">SC</option>
				    	<option value="SD">SD</option>
				    	<option value="TN">TN</option>
				    	<option value="TX">TX</option>
				    	<option value="UT">UT</option>
				    	<option value="VT">VT</option>
				    	<option value="VA">VA</option>
				    	<option value="WA">WA</option>
				    	<option value="WV">WV</option>
				    	<option value="WI">WI</option>
						<option value="WY">WY</option>
				    </select>
				    <button type="submit" value="Submit">Apply</button>
			 	</form>
			</div>
		</div>


		<?php if ( !isset($nothingpassed) ) { ?>
			<section id="single-sidebar-contents" class="left max-width">
		<?php } else { ?>
			<section id="single-sidebar-contents" class="archive-category-feed left max-width">
		<?php } ?>

			<div id="events-nav" class="secondary-nav full-width">
				<nav>
					<!-- <?php wp_nav_menu( array( 'theme_location' => 'events-nav' ) ); ?> -->
					<div class="menu-events-filter-container">
						<ul id="menu-events-filter" class="menu">
							<li id="menu-item-710" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-710"><a href="/events/event/">All Events</a></li>
							<?php $have_posts = [ 'post_type' => 'event', 'posts_per_page' => -1, 'meta_key' => 'format', 'meta_value' => 'seminar', ]; query_posts( $have_posts ); ?>
							<?php if ( have_posts() ) { ?>
								<li id="menu-item-713" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-713"><a href="/events/event/?format=seminar">Roundtables/Seminars</a></li>
							<?php } ?>
							<?php $have_posts = [ 'post_type' => 'event', 'posts_per_page' => -1, 'meta_key' => 'format', 'meta_value' => 'webinar', ]; query_posts( $have_posts ); ?>
							<?php if ( have_posts() ) { ?>
								<li id="menu-item-711" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-711"><a href="/events/event/?format=webinar">Webinars</a></li>
							<?php } ?>
							<?php $have_posts = [ 'post_type' => 'event', 'posts_per_page' => -1, 'meta_key' => 'format', 'meta_value' => 'engagements', ]; query_posts( $have_posts ); ?>
							<?php if ( have_posts() ) { ?>
								<li id="menu-item-714" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-714"><a href="/events/event/?format=engagements">Speaking Engagements</a></li>
							<?php } ?>
							<?php $have_posts = [ 'post_type' => 'event', 'posts_per_page' => -1, 'meta_key' => 'format', 'meta_value' => 'tradeshow', ]; query_posts( $have_posts ); ?>
							<?php if ( have_posts() ) { ?>
								<li id="menu-item-712" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-712"><a href="/events/event/?format=tradeshow">Tradeshows</a></li>
							<?php } ?>
						</ul>
					</div>
					<div style="clear: both"></div>
				</nav> 
			</div>


			<?php
				query_posts( $args ); 
				if ( have_posts() ) : while ( have_posts() ) : the_post();
			?>
						<?php get_template_part( 'template-parts/content', 'event' ); ?>
						<?php endwhile; ?>
						<div style="clear: both"></div>
						<?php the_posts_pagination( array(
							'prev_text'          => __( 'Previous page', 'twentysixteen' ),
							'next_text'          => __( 'Next page', 'twentysixteen' ),
							'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
						) );
					else : ?>
						<article>
							<h2>No Events Were Found</h2>
							<p>Other events you might be intrested found below.</p>
						</article>
						<hr>
						<h2></h2>
						<?php 
							$argss = [ 
								'post_type' => 'event', 
								'order' => 'ASC', 
								'posts_per_page' => 5,
						    ];
							query_posts( $argss );
							if ( have_posts() ) : while ( have_posts() ) : the_post();
						?>
							<?php get_template_part( 'template-parts/content', 'event' ); ?>
						<?php endwhile; ?><?php endif; ?>
					<?php endif; ?>
			</section>
		<aside id="single-sidebar" class="right widget-area-container events-sidebar">
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('events-sidebar')) : else : ?>
				<p><strong>Widget Ready</strong></p>  
			<?php endif; ?> 
		</aside>
		<div style="clear: both"></div>
	</div>
	
</main>

<?php get_footer(); ?>