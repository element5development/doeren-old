<?php /*
Template Name: Home Page
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<div id="home-contents" class="max-width">
		<div id="home-slider">
			<?php echo do_shortcode('[rev_slider alias="home-slider"]'); ?>
		</div>
		<div id="home-feed-container" class="full-width">
			<div id="viewpoint-home-feed" class="one-third">
				<a class="feed-title" href="/viewpoint">VIEWpoint</a>
				<div class="home-posts-container full-width">
					<?php get_template_part( 'template-parts/content', 'home-viewpoint' ); ?>
				</div>
			</div>
			<div id="events-home-feed" class="one-third">
				<a class="feed-title" href="/events/event/">Events</a>
				<div class="home-posts-container full-width">
					<?php get_template_part( 'template-parts/content', 'home-events' ); ?>
				</div>
			</div>
			<div id="resources-home-feed" class="one-third">
				<a class="feed-title" href="/resources">Resources</a>
				<div class="home-posts-container full-width">
					<?php get_template_part( 'template-parts/content', 'home-resources' ); ?>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>
	</div>
	
	<?php get_template_part( 'template-parts/content', 'newsletter' ); ?>
		

</main>

<?php get_footer(); ?>