<?php /*
TEMPLATE FOR SEARCH RESULTS
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<div class="page-header max-width" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/img/search-header.jpg);">
		<div class="page-header-contents">
			<h1 class="page-title">Search</h1>
			<p class="page-desctiption">
				<?php printf( __( 'Below are the results found for your search of "%s"', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?>
			</p>
		</div>
	</div>

	<section id="single-column-contents" class="max-width search-feed">

		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post();
			switch_to_blog($post->blog_id);
			get_template_part( 'template-parts/content', 'search' );
			restore_current_blog();
		endwhile; ?>
		<div style="clear: both"></div>
		<?php else : ?>
			<article>
				<h2>Nothing was Found, Try Again.</h2>
				<?php get_search_form(); ?>
			</article>
		<?php endif; ?>

	</section>


</main>

<?php get_footer(); ?>