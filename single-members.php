<?php /*
TEMPLATE FOR SINGLE CUSTOM POST TYPE "MEMBERS"
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<div class="back-to-parent max-width">
		<a class="back-page" href="/about-doeren_mayhew/directory/">Back to Directory</a>
	</div>

	<div id="page-contents-container" class="max-width">

	<aside id="single-sidebar" class="left team">
		<div class="grey-bg">
			<div class="featured member"><?php the_post_thumbnail(); ?></div>
			<div class="information">
				<h3><?php the_title(); ?></h3>
				<?php if( get_field('credentials') ): ?>
					<h4><?php the_field( 'credentials' ) ?></h4>
				<?php endif; ?>
				<p><?php the_field( 'title' ) ?></p>	
			</div>
			<div class="contacts">
				<a class="phone-icon" href="tel:<?php the_field( 'phone_number' ) ?>">
					<div class="hover-text"><?php the_field( 'phone_number' ) ?></div>
				</a>
				<a class="email-icon" href="mailto:<?php the_field( 'email' ) ?>">
					<div class="hover-text"><?php the_field( 'email' ) ?></div>
				</a>
				<a class="vcard-icon" href="<?php the_field( 'vcard' ) ?>">
					<div class="hover-text">Vcard Download</div>
				</a>
				<a class="pdf-icon" href="<?php the_field( 'pdf_download' ) ?>">
					<div class="hover-text">PDF Resume</div>
				</a>
				<?php if( get_field('linkedin') ): ?>
					<a class="linkedin-icon" target="_blank" href="<?php the_field( 'linkedin' ) ?>">
						<div class="hover-text">LinkedIn Page</div>
					</a>
				<?php endif; ?>	
			</div>
			<div class="contents">
				<p><span>Location</span><br/><?php the_field( 'member_locations' ) ?><p>
				<p><span>Industry</span><br/><?php the_field( 'member_industry' ) ?></p>
				<p><span>Area of Expertise</span><br/><?php the_field( 'member_expertise' ) ?></p>
			</div>
		</div>
		<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('single-left-sidebar')) : else : ?>
		<?php endif; ?>  
	</aside>

		<section id="single-sidebar-contents" class="right">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</section>

		<div style="clear: both"></div>

	</div>
</main>

<?php get_footer(); ?>