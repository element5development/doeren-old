<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES & CATEGORY FOR CASE STUDIES
*/ ?>

<?php get_header(); ?>

<main class="max-width">

	<div class="page-header max-width" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/img/case-study-header.jpg');">
		<div class="page-header-contents">
			<div class="header-center">
				<h1 class="page-title">Case Studies</h1>
				<p class="page-desctiption"></p>
			</div>
		</div>
	</div>

	<div id="page-contents-container" class="max-width">
		<section class="archive-category-feed max-width">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'case-studies' ); ?>
				<?php endwhile; ?>
			<div style="clear: both"></div>
			<?php the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );
			else : ?>
				<article>
					<h2>No Case Studies Were Found</h2>
				</article>
			<?php endif; ?>
		</section>
		<div style="clear: both"></div>
	</div>

</main>

<?php get_footer(); ?>