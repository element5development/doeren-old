<?php /*
Template Name: Additional VIEWpoint Images
USED FOR DISPLAYING LOAD MORE OPTION
*/ ?>

<?php
	/*--SKIP ALREADY LOADED POSTS--*/
    $offset = htmlspecialchars(trim($_GET['offset']));
    if ($offset == '' or is_null($offset) ) {
	    $offset = 6;
	}

    $args = array(
	    'posts_per_page' => $offset,
	    'offset' => 0,
		// 'meta_key'		=> 'place_in_no_image_feed',
		// 'meta_value'	=> 'no'
	);

	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			get_template_part( 'template-parts/content', 'viewpoint-images' );
		}
	}
?>