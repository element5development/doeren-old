<?php /*
Template Name: Left Sidebar
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<?php get_template_part( 'template-parts/content', 'page-header' ); ?>

	<div id="page-contents-container" class="max-width">

		<section id="single-sidebar-contents" class="right">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</section>

		<aside id="single-sidebar" class="left widget-area-container">
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('single-left-sidebar')) : else : ?>
				<p><strong>Widget Ready</strong></p>  
			<?php endif; ?>  
		</aside>

		<div style="clear: both"></div>

	</div>
</main>

<?php get_footer(); ?>