<?php /*
DEFAULT PAGE TEMPLATE (SINGLE COLUMN)
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<?php if( !is_page('151') && !is_page('152') ) { ?>	
		<?php get_template_part( 'template-parts/content', 'page-header' ); ?>
	<?php } ?>

	<?php get_template_part( 'template-parts/content', 'secondary-nav' ); ?>

	<section id="single-column-contents" class="max-width site<?php echo get_current_blog_id(); ?>">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
		<!--TIMELINE FEED-->
		<?php if( is_page('35') ) { ?>	
			<?php get_template_part( 'template-parts/content', 'timeline' ); ?>
		<?php } ?>
		<!--TIMELINE FEED-->
		<?php if( is_page('history') && get_current_blog_id() == 10 ) { ?>	
			<?php get_template_part( 'template-parts/content', 'timeline' ); ?>
		<?php } ?>
		<!--MEMBERS FEED & FILTERS-->
		<?php if( is_page('36') ) { ?>
			<?php get_template_part( 'template-parts/content', 'directory' ); ?>
		<?php } ?>
		<!--LOCATIONS FEED-->
		<?php if( is_page('38') ) { ?>
			<?php get_template_part( 'template-parts/content', 'locations' ); ?>
		<?php } ?>
		<!--CAREERS PAGES-->

		<!--SERVICES-->
		<?php if( is_page('72') ) { ?>
			<?php get_template_part( 'template-parts/content', 'services' ); ?>
		<?php } ?>
		<!--SPECILIZATIONS-->
		<?php if( is_page('55') ) { ?>
			<?php get_template_part( 'template-parts/content', 'specilizations' ); ?>
		<?php } ?>
	</section>
	<br clear="all">
	<div class="clearfix"></div>
</main>

<?php get_footer(); ?>