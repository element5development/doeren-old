<?php /*
TEMPLATE USED FOR MAIN BLOG (VIEWPOINT)
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<?php if ( have_posts() ) : ?>

		<?php if ( is_home() && ! is_front_page() ) : ?>
			<div class="page-header max-width" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/img/blog-bg.png);">
				<div class="page-header-contents">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/viewpoint-logo.png" />
				</div>
			</div>
		<?php endif; ?>


		<div id="page-contents-container" class="max-width">
			<section id="single-sidebar-contents" class="left">

				<div class="full-width">
					<div id="viewpoint-featured">
						<!--Single Featured Post -->
						<?php get_template_part( 'template-parts/content', 'viewpoint-featured' ); ?>
					</div>
				</div>
				<div class="full-width viewpoint-post-feeds">
					<div class="full-width">
						<div id="viewpoint-images">
							<!-- Feed with Images -->
							<div style="clear: both"></div>
						</div>
						<div style="clear: both"></div>
						<a href="#" id="load-more-viewpoint-images" class="secondary-button show">Load More</a>
					</div>
					<div style="clear: both"></div>
				</div>

			</section>
			<aside id="single-sidebar" class="right widget-area-container">
				<div class="widget categories-widget">
					<h2 class="right-sidebar-title">Categories</h2>
					<ul>
						<li class="top-level has-child">Services
							<ul class="child-categories">
								<?php $child_categories=get_categories(array( 'parent' => 24 )); ?>
								<?php foreach ( $child_categories as $child ) : ?>
									<li><a href="<?php echo get_category_link($child->term_id) ?>"><?php echo $child->cat_name; ?></a></li>
								<?php endforeach; ?>
							</ul>
						</li>
						<li class="top-level has-child">Specializations
							<ul class="child-categories">
								<?php $child_categories=get_categories(array( 'parent' => 23 )); ?>
								<?php foreach ( $child_categories as $child ) : ?>
									<li><a href="<?php echo get_category_link($child->term_id) ?>"><?php echo $child->cat_name; ?></a></li>
									<?php if ( $child->term_id == 40 ) : ?>
										<li><a href="<?php echo get_category_link(701) ?>"><?php echo get_cat_name(701); ?></a></li>
									<?php endif; ?>
								<?php endforeach; ?>
							</ul>
						</li>
						<li class="top-level"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">View All</a></li>
					</ul>
				</div>
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('single-right-sidebar')) : else : ?>
					<p><strong>Widget Ready</strong></p>  
				<?php endif; ?>  
			</aside>
			<div style="clear: both"></div>
		</div>


	<?php endif; ?>


</main>

<?php get_footer(); ?>