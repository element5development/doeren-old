<?php /*
TEMPLATE FOR SINGLE CUSTOM POST TYPE "JOBS"
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<div class="back-to-parent max-width">
		<a class="back-page" href="/careers/current-openings/">Back to Job Openings</a>
	</div>

	<div id="page-contents-container" class="max-width">
		<aside id="single-sidebar" class="left jobs">
			<div class="grey-bg">
				<h1><?php the_title(); ?></h1>
				<h3>Location: <?php the_field( 'job_location' ) ?><br />Service Line: <?php the_field( 'job_specialty' ) ?></h3>
				<a class="primary-button" href="/careers/apply-online/?position=<?php the_title(); ?>&location=<?php the_field( 'job_location' ) ?>">Apply Online</a>
			</div>
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('single-left-sidebar')) : else : ?>
			<?php endif; ?>  
		</aside>
		<section id="single-sidebar-contents" class="right">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</section>
		<div style="clear: both"></div>
	</div>

</main>

<?php get_footer(); ?>