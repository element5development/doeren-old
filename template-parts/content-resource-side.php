<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "RESOURCES" WHEN FOUND IN THE SIDEBAR
*/ ?>

<?php
	$cookie_name = "resource-download";
?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<?php
	if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource') ) { ?>
		<a class="pop-up-form read-more">
	<?php } else { ?>
		<a class="read-more" href="<?php the_permalink(); ?>">
	<?php }
?>

<article id="post-<?php the_ID(); ?>" class="resource-side" style="background-image: url(<?php echo $src[0]; ?> )">
	<div class="post-gradient"></div>

	<div class="resource-contents">
		<div class="resource-format"><?php the_field( 'format' ) ?></div>
		<?php
			if(!isset($_COOKIE[$cookie_name])) { ?>
				<p class="entry-header pop-up-form">
					<?php the_title(); ?>
				</p>
			<?php } else { ?>
				<a href="<?php the_permalink(); ?>">
					<p class="entry-header">
						<?php the_title(); ?>
					</p>
				</a>
			<?php }
		?>
		<?php
			if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource') ) { ?>
				<a class="pop-up-form read-more" data-link="<?php the_permalink(); ?>">Sign in to read <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a>
			<?php } else { ?>
				<a class="read-more" href="<?php the_permalink(); ?>">Read the rest <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a>
			<?php }
		?>
	</div>
</article>

</a>