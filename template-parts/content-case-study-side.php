<?php /*
TEMPLATE FOR DISPLAYING CASE STUDIES WHEN FOUND IN THE SIDEBAR
*/ ?>

<article id="post-<?php the_ID(); ?>" class="viewpoint-side case-study-side">
	<a href="<?php the_permalink(); ?>">
		<p class="entry-header">
			<?php 
          $title  = the_title('','',false);
          if(strlen($title) > 65) {
              echo trim(substr($title, 0, 65)).'...';
          } else {
              echo $title;
          }
        ?>
		</p>
	</a>
	<a class="primary-button" href="<?php the_permalink(); ?>">View Case Study</a>
</article>