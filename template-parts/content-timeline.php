<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "TIMELINE"
*/ ?>

<div class="timeline-feed feed-cotainer">
	<?php
	  query_posts( array( 'post_type' => 'timelines', 'order' => 'DEC', 'posts_per_page' => -1 ) );
	  if ( have_posts() ) : while ( have_posts() ) : the_post();
	?>

		<div class="timeline-event <?php echo (++$j % 2 == 0) ? 'even' : 'odd'; ?>">
			<div id="timeline-image">  
			</div>
			<div class="timeline-content">
				<div class="featured"><?php the_post_thumbnail(); ?></div>
				<h2><?php the_title(); ?></h2>
				<div class="entry">
					<?php the_content(); ?>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>

	<?php endwhile; endif; wp_reset_query(); ?>
	<div style="clear: both"></div>
</div>

