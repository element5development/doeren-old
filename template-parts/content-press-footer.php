<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "PRESS" WHEN DISPLAYED IN THE FOOTER
*/ ?>

<article id="post-<?php the_ID(); ?>" class="post-feed press">

	<a class="entry-header" href="<?php echo get_permalink(); ?>">
		<p>
			<?php echo substr(the_title($before = '', $after = '', FALSE), 0, 65); ?>...
		</p>
	</a> 

</article>
