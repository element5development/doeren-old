<?php /*
TEMPLATE FOR DISPLAYING POSTS (VIEWPOINT) WEN FOUND IN THE SIDEBAR
*/ ?>

<article id="post-<?php the_ID(); ?>" class="viewpoint-side">
	
	<div class="post-feed-date">
		<?php the_time('m.d.y') ?>
	</div>
	<a href="<?php the_permalink(); ?>">
		<p class="entry-header">
			<?php 
          $title  = the_title('','',false);
          if(strlen($title) > 65) {
              echo trim(substr($title, 0, 65)).'...';
          } else {
              echo $title;
          }
        ?>
		</p>
	</a>

</article>
