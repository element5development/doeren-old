<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "RESOURCES"
*/ ?>

<?php
	$cookie_name = "resource-download";
?>

<article id="post-<?php the_ID(); ?>" class="post-feed archieves-post resource grey-bg">

	<?php
		if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource') ) { ?>
			<a class="pop-up-form read-more pop-up-form-read-more full-cover" data-link="<?php the_permalink(); ?>"></a>
		<?php } else { ?>
			<a class="read-more pop-up-form-read-more full-cover" href="<?php the_permalink(); ?>"></a>
		<?php }
	?>

	<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>
	<div class="post-feed-image" style="background-image: url(<?php echo $src[0]; ?> )">
		<div class="post-gradient"></div>
	</div>

	<div class="resource-contents">
		<div class="resource-format"><?php the_field( 'format' ) ?></div>
		<?php
			if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource') ) { ?>
				<h3 class="entry-header pop-up-form">
					<?php the_title(); ?>
				</h3>
			<?php } else { ?>
				<a class="pop-up-form-read-more" href="<?php the_permalink(); ?>" data-link="<?php the_permalink(); ?>">
					<h3 class="entry-header">
						<?php the_title(); ?>
					</h3>
				</a>
			<?php }
		?>
		<div class="post-feed-excerpt"><?php the_field( 'description' ) ?></div>
		<hr>

		<?php
			if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource') ) { ?>
				<a class="pop-up-form read-more pop-up-form-read-more" data-link="<?php the_permalink(); ?>">Sign in to read <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a>
			<?php } else { ?>
				<a class="read-more" href="<?php the_permalink(); ?>">Read the rest <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a>
			<?php }
		?>
	
	</div>
	<div style="clear: both"></div>

</article>