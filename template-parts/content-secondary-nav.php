<?php /*
TEMPLATE FOR DISPLAYING SECONDARY NAVIGATION 
*/ ?>

<!-- ABOUT PAGE AND CHILD PAGES -->
<?php if( is_page('about-doeren_mayhew') || is_child('about-doeren_mayhew') ) { ?>
<div id="about-nav" class="secondary-nav max-width">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'about-nav' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- CAREERS PAGE AND CHILD PAGES -->
<?php if( is_page('careers') || is_child('careers') || is_child('college-hires') ) { ?>
<div id="careers-nav" class="secondary-nav max-width">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'career-nav' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- INTERNAL AUDIT & ACCOUNTING, AUDIT AND ASSURANCE -->
<?php if( is_page('accounting-audit-assurance') || is_child('accounting-audit-assurance') || is_child('internal-audit') ) { ?>
<div id="accounting-nav" class="secondary-nav max-width service-navigation">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'service-accounting-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- CFO & BUSINESS ADVISORY -->
<?php if( is_page('business-advisory') || is_child('business-advisory') || is_child('cfo-services') ) { ?>
<div id="business-advisory-nav" class="secondary-nav max-width service-navigation">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'service-business-advisory-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- DENTAL CPA -->
<?php if( is_page('dental-cpa') || is_child('dental-cpa') ) { ?>
<div id="dental-nav" class="secondary-nav max-width service-navigation">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'service-dental-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- IT ASSURANCE & SECURITY -->
<?php if( is_page('it-advisory-and-cybersecurity') || is_child('it-advisory-and-cybersecurity') ) { ?>
<div id="it-sssurance-nav" class="secondary-nav max-width service-navigation">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'service-it-sssurance-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- MERGERS & ACQUISITIONS -->
<?php if( is_page('mergers-acquisitions') || is_child('mergers-acquisitions') ) { ?>
<div id="mergers-nav" class="secondary-nav max-width service-navigation">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'service-mergers-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- TAX INCENTIVES & TAX -->
<?php if( is_page('tax-compliance-services') || is_child('tax-compliance-services') || is_child('tax-incentives') ) { ?>
<div id="tax-nav" class="secondary-nav max-width service-navigation">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'service-tax-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- FINANCIAL INSTITUTIONS -->
<?php if( is_page('financial-institutions') || is_child('financial-institutions') || in_array( 12406, get_ancestors($post->ID, 'page')) ) { ?>
<div id="financial-nav" class="secondary-nav max-width service-navigation">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'financial-institutions-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- Valuation & Litigation Support -->
<?php if( is_page('valuation-litigation-support-services') || is_child('valuation-litigation-support-services') ) { ?>
<div id="valuation-nav" class="secondary-nav max-width">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'valuation-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>
<!-- WHY DOEREN MAYHEW -->
<?php if( is_page('why-doeren-mayhew') || is_child('why-doeren-mayhew') ) { ?>
<div id="why-doren-nav" class="secondary-nav max-width">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'why-doeren-nav', 'link_after' => '<span class="arrow"><img src="/wp-content/themes/doeren-mayhew/img/icon-arrow-small-down-grey.svg" /></span>' ) ); ?>
		<div style="clear: both"></div>
	</nav>
</div>
<?php } ?>