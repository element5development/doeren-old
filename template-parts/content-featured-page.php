<?php /*
TEMPLATE FOR DISPLAYING TAX REFORM PAGE IN THE MEGA MENU
*/ ?>

<a class="featured-resouce-link" href="<?php the_permalink(); ?>">
	<article id="post-<?php the_ID(); ?>" class="post-feed">
	<a class="featured-resouce-link read-more full-cover" href="<?php the_permalink(); ?>"></a>
		<div class="one-half">
			<?php $src = get_the_post_thumbnail_url(); ?>
			<div class="post-feed-image" style="background-image: url(<?php echo $src; ?> )">
			</div> 
		</div>
		<div class="one-half featured-post-contents">
			<h3 class="entry-header pop-up-form">
				<?php echo esc_html( get_the_title() ); ?>
			</h3>
			<div class="post-feed-excerpt">
				<?php the_field( "page_description" ); ?>
			</div> 
			<a class="read-more" href="<?php the_permalink(); ?>">Read More <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a>		
		</div>
		<div style="clear: both"></div>
	</article>
</a>	