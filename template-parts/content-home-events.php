<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "EVENTS" ON THE HOMEPAGE
*/ ?>

<?php $args = array(
    'posts_per_page' => 20,
    'offset' => 0,
    'post_type' => 'event'
);

$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
		$the_query->the_post(); ?>
		<a class="home-event" href="<?php the_permalink(); ?>">
			<article id="post-<?php the_ID(); ?>" class="post-feed full-width <?php echo (++$j % 2 == 0) ? 'even' : 'odd'; ?>">
				
				<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>
				<?php if ( get_field('preview_image') ) {
					$previewimg = get_field('preview_image');
				} else {
					$previewimg = $src[0];
				} ?>

				<div class="post-feed-image" style="background-image: url(<?php echo $previewimg; ?> )">
					<div class="post-gradient"></div>
				</div>
				<div class="event-contents">
					<div class="event-main-date">
						<span class="month"><?php echo eo_get_the_start('M') ?></span>	
						<span class="day"><?php echo eo_get_the_start('j') ?></span>
						<span class="year"><?php echo eo_get_the_start('Y') ?></span>
					</div>
					<div class="event-info">
						<div class="event-format"><?php the_field( 'format' ) ?></div>
						<p class="entry-header">
							<?php 
								$title  = the_title('','',false);
								if(strlen($title) > 100) {
								    echo trim(substr($title, 0, 100)).'...';
								} else {
								    echo $title;
								}
							?>
						</p>
					</div>
					<div style="clear: both"></div>
				</div>

			</article>
		</a>
	<?php }
} ?>
