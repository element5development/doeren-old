<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "PRESS"
*/ ?>

<article id="post-<?php the_ID(); ?>" class="post-feed archieves-post press grey-bg">

	<div class="post-feed-date"><?php the_time('Y') ?></div> 
	<a class="entry-header" href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3></a> 
	<div class="post-feed-excerpt"><?php the_excerpt(); ?></div>
	<hr>
	<a class="read-more" href="<?php the_permalink(); ?>">Read the rest <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a> 

</article>
