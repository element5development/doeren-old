<?php /*
TEMPLATE TO DISPLAY CUSTOM POST TYPE "EVENTS"
*/ ?>

<article id="post-<?php the_ID(); ?>" class="post-feed archieves-post event grey-bg">

	<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>
	<?php if ( get_field('preview_image') ) {
		$previewimg = get_field('preview_image');
	} else {
		$previewimg = $src[0];
	} ?>

	<div class="post-feed-image" style="background-image: url( <?php echo $previewimg; ?> )">
		<div class="post-overlay"></div>
	</div>

	<div class="event-contents">
		<div class="event-main-date">
			<span class="month"><?php echo eo_get_the_start('M') ?></span>	
			<span class="day"><?php echo eo_get_the_start('d') ?></span>
			<span class="year"><?php echo eo_get_the_start('Y') ?></span>
		</div>
		<div class="event-info">
			<div class="event-format"><?php the_field( 'format' ) ?></div>
			<h3 class="entry-header">
				<?php the_title(); ?>
			</h3>
			<div class="event-location">
				<?php the_field( 'venue_name' ) ?>
				<?php if ( get_field('city') ) { ?>
					, <?php the_field( 'city' ) ?>, <?php the_field( 'state' ) ?>
				<?php } ?>

			</div>

			<?php if ( eo_get_the_start('j') != eo_get_the_end('j') ) { ?>
				<div class="event-date"><?php echo eo_get_the_start('n.j.y') ?> - <?php echo eo_get_the_end('n.j.y') ?></div>
			<?php } else { ?>
				<div class="event-date"><?php echo eo_get_the_start('g:i a')." to ".eo_get_the_end('g:i a'); ?> <?php the_field('event_timezone'); ?></div>
			<?php } ?>

		</div>
		<div style="clear: both"></div>
	</div>
	<div style="clear: both"></div>

	<a id="event-feed-link" href="<?php the_permalink(); ?>"></a>

</article>