<?php /*
TEMPLATE FOR DISPLAYING POST SET AS FEATURED IN THE MEGA MENU
*/ ?>

<?php
	$cookie_name = "resource-download";
?>

<?php if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource')) { ?>
	<a class="pop-up-form">
<?php } else { ?>
	<a class="featured-resouce-link" href="<?php the_permalink(); ?>">
<?php } ?>

	<article id="post-<?php the_ID(); ?>" class="post-feed">
		<?php if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource')) { ?>
			<a class="pop-up-form read-more full-cover pop-up-form-read-more" data-link="<?php the_permalink(); ?>"></a>
		<?php } else { ?>
			<a class="featured-resouce-link read-more full-cover" href="<?php the_permalink(); ?>"></a>
		<?php } ?>
		<div class="one-half">
			<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>
			<div class="post-feed-image" style="background-image: url(<?php echo $src[0]; ?> )">
			</div> 
		</div>
		<div class="one-half featured-post-contents">
			<h3 class="entry-header pop-up-form">
				<?php 
					$title  = the_title('','',false);
					if(strlen($title) > 65) {
					    echo trim(substr($title, 0, 65)).'...';
					} else {
					    echo $title;
					}
				?>
			</h3>
			<div class="post-feed-excerpt">
				<?php 
					$excerpt = get_the_excerpt();
		      		$excerpt = strip_tags($excerpt);
		      		echo substr($excerpt, 0, 100);
		      	?>
				...
			</div> 
			<?php if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource')) { ?>
				<a class="pop-up-form read-more pop-up-form-read-more" data-link="<?php the_permalink(); ?>">Sign in to read <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a>
			<?php } else { ?>
				<a class="read-more" href="<?php the_permalink(); ?>">Read Full Article <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a> 
			<?php } ?>		
		</div>
		<div style="clear: both"></div>
	</article>
</a>	