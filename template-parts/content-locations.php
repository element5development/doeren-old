<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "LOCATIONS"
*/ ?>

<div class="location-feed feed-cotainer">
	<?php
		query_posts( array( 'post_type' => 'locations', 'order' => 'ASC', 'posts_per_page' => -1 ) );
		if ( have_posts() ) : while ( have_posts() ) : the_post();
	?>

		<div class="location one-third">
			<?php if( get_the_ID() == '195' ): ?>
				<div class="headquarters">headquarters</div>
			<?php endif; ?>
			<div class="featured"><?php the_post_thumbnail(); ?></div>
			<h2><?php the_title(); ?></h2>
			<hr>
			<div class="address">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-map-pointer-blue.svg" />
				<?php the_field( 'address' ) ?>
			</div>
			<div class="phone">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-phone-blue.svg" />
				<a href="tel:<?php the_field( 'phone' ) ?>"><?php the_field( 'phone' ) ?></a>
			</div>
			<a class="secondary-button" href="/contact">Contact Us</a>
			<?php if( get_field('state') ): ?>
				<a class="secondary-button" href="<?php echo get_permalink(); ?>">Visit <?php the_field( 'state' ) ?> Page</a>
			<?php endif; ?>
		</div>

	<?php endwhile; endif; wp_reset_query(); ?>
	<div style="clear: both"></div>
</div>

