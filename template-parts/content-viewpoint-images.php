<?php /*
TEMPLATE FOR DISPLAYING POSTS (VIEWPOINT) THAT HAVE FEATURED IMAGES
*/ ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<?php
	if ( $src[0] == '') {
		$site = get_bloginfo('stylesheet_directory'); 
		$featurediamge = $site.'/img/default-blog-featured.png';
	} else {
		$featurediamge = $src[0];
	}
?>


<article id="post-<?php the_ID(); ?>" class="post-feed viewpoint-image one-half">

	<div class="post-feed-date">
		<span class="month"><?php the_time('M') ?></span>	
		<span class="day"><?php the_time('j') ?></span>
		<span class="year"><?php the_time('Y') ?></span>
	</div>
	<div class="post-feed-image" style="background-image: url(<?php echo $featurediamge; ?> )">
		<a href="<?php the_permalink(); ?>">
			<h3 class="entry-header">
				<?php echo substr(the_title($before = '', $after = '', FALSE), 0, 100); ?>
			</h3>
		</a>
		<div class="post-gradient"></div>
	</div>
	<div class="viewpoint-image-contents">
		<div class="post-feed-excerpt">
			<?php if ( get_field('description') ) {
				the_field('description');
			} else {
				$content = get_the_excerpt(); 
				echo substr($content, 0, 250);
			} ?>
		</div> 
		<a class="read-more" href="<?php the_permalink(); ?>">Read Full Article <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a> 
	</div>

</article>
