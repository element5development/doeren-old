<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "EVENTS" WHEN FOUDN IN SIDEBAR
*/ ?>

<article id="post-<?php the_ID(); ?>" class="event-side">

	<div class="event-contents">
		<div class="event-main-date">
			<span class="month"><?php echo eo_get_the_start('M') ?></span>	
			<span class="day"><?php echo eo_get_the_start('j') ?></span>
			<span class="year"><?php echo eo_get_the_start('Y') ?></span>
		</div>
		<div class="event-info">
			<div class="event-format"><?php the_field( 'format' ) ?></div>
			<a href="<?php the_permalink(); ?>">
				<p class="entry-header">
					<?php the_title(); ?>
				</p>
			</a>
		</div>
		<div style="clear: both"></div>
	</div>

</article>
