<?php /*
TEMPLATE FOR DISPLAYING ANY POST TYPE IN SEARCH RESULTS
*/ ?>

<article id="post-<?php the_ID(); ?>" class="post-feed one-third">

	<div class="search-title">
		<div class="post-format">
			<?php $post_type = get_post_type_object( get_post_type($post) );
			echo $post_type->labels->singular_name ; ?>
		</div>
		<?php if ($post_type->labels->singular_name == 'Post') : ?>
			<div class="date">
				<?php the_date('M d, Y'); ?>
			</div>
		<?php endif; ?>
		<a href="<?php the_permalink(); ?>">
			<h3 class="entry-header">
				<?php 
					$title  = the_title('','',false);
					if(strlen($title) > 75) {
					    echo trim(substr($title, 0, 75)).'...';
					} else {
					    echo $title;
					}
				?>
			</h3>
		</a>
	</div>
	<div class="search-contents">
		<div class="post-feed-excerpt">
			<?php
				$excerpt  = get_the_excerpt();
				if(strlen($excerpt) > 250) {
				    echo trim(substr($excerpt, 0, 245)).'...';
				} else {
				    echo $excerpt;
				}
			?>
		</div> 
		<a class="read-more" href="<?php the_permalink(); ?>">View <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a> 
	</div>

</article>

