<?php /*
TEMPLATE FOR DISPLAYING POST SET AS FEATURED IN THE MEGA MENU
*/ ?>

<article id="post-<?php the_ID(); ?>" class="post-feed">
	<div class="one-half">
		<div class="post-feed-date">
			<span class="month"><?php the_time('M') ?></span>	
			<span class="day"><?php the_time('j') ?></span>
			<span class="year"><?php the_time('Y') ?></span>
		</div>
		<?php 
			//USE FEATURED IAMGE OTHERWISE USE DEFAULT IAMGE
			$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); 
			if ( $src[0] == "") {
				$featuredimg = "/wp-content/themes/doeren-mayhew/img/default-blog-image.png";
			} else {
				$featuredimg = $src[0];
			}	
		?>
		<div class="post-feed-image" style="background-image: url(<?php echo $featuredimg; ?> )">
		</div> 
	</div>
	<div class="one-half featured-post-contents">
		<a href="<?php the_permalink(); ?>">
			<h3 class="entry-header">
				<?php 
					$title  = the_title('','',false);
					if(strlen($title) > 65) {
					    echo trim(substr($title, 0, 65)).'...';
					} else {
					    echo $title;
					}
				?>
			</h3>
		</a>
		<div class="post-feed-excerpt">
			<?php 
				$excerpt = get_the_excerpt();
	      		$excerpt = strip_tags($excerpt);
	      		echo substr($excerpt, 0, 100);
	      	?>
			...
		</div> 
		<a class="read-more" href="<?php the_permalink(); ?>">Read Full Article <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a> 
	</div>
	<div style="clear: both"></div>
</article>