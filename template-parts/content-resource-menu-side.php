<?php /*
TEMPLATE FOR DISPLAYING POSTS (VIEWPOINT) WEN FOUND IN THE SIDEBAR
*/ ?>

<?php
	$cookie_name = "resource-download";
?>

<article id="post-<?php the_ID(); ?>" class="viewpoint-side">
	
	<div class="post-feed-date">
		<?php the_time('m.d.y') ?>
	</div>
	<?php
		if(!isset($_COOKIE[$cookie_name]) && get_field('gated_resource') ) { ?>
			<p class="entry-header pop-up-form pop-up-form-read-more" data-link="<?php the_permalink(); ?>">
				<?php 
					$title  = the_title('','',false);
					if(strlen($title) > 65) {
					    echo trim(substr($title, 0, 65)).'...';
					} else {
					    echo $title;
					}
				?>
			</p>
		<?php } else { ?>
			<a href="<?php the_permalink(); ?>">
				<p class="entry-header">
					<?php 
						$title  = the_title('','',false);
						if(strlen($title) > 65) {
						    echo trim(substr($title, 0, 65)).'...';
						} else {
						    echo $title;
						}
					?>
				</p>
			</a>
		<?php }
	?>

</article>
