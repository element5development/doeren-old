<?php /*
TEMPLATE FOR DISPLAYING STYLED LIST OF CHILD PAGES
*/ ?>

<div class="specializations-menu max-width">
	<?php switch_to_blog(1); ?>
		<?php wp_nav_menu( array( 'theme_location' => 'specialization-nav' ) ); ?>
	<?php restore_current_blog(); ?>
	<div style="clear: both"></div>
</div>

<div style="clear: both"></div>
