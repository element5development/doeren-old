<?php /*
TEMPLATE FOR DISPLAYING CUSTOM POST TYPE "JOBS"
*/ ?>

<?php
	$location = $_GET["location"];
	$specialty = $_GET["specialty"];

	if ( $location == NULL and $specialty == NULL) {
		/*NO VARIABLES PASSED*/
		$args = [ 
			'post_type' => 'jobs', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
	    ];
	} elseif ( $location == 'none' ) {
	    /*NO LOCATION*/
		$args = [ 
			'post_type' => 'jobs', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'meta_key'		=> 'job_specialty',
			'meta_value'	=> $specialty,
	    ];
	} elseif ( $specialty == 'none' ) {
	    /*NO SPECIALTY*/
		$args = [ 
			'post_type' => 'jobs', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'meta_key'		=> 'job_location',
			'meta_value'	=> $location,
	    ];
	} else {
	    /*EVERYTHING PASSED*/
		$args = [ 
			'post_type' => 'jobs', 
			'order' => 'ASC', 
			'posts_per_page' => -1,
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'key'	 	=> 'job_specialty',
					'value'	  	=> $specialty,
					'compare' 	=> '=',
				),
				array(
					'key'	  	=> 'job_location',
					'value'	  	=> $location,
					'compare' 	=> '=',
				),
			),
	    ];
	}
?>

<div class="filter">
	<div class="filter-search events">
		<form id="category-filter" method="get" action="/careers/current-openings/">
		    <h2>Filter By:</h2>
		    <select name="location">
		    	<option value="none">Location</option>
		    	<option value="Charlotte, NC">Charlotte, NC</option>
		    	<option value="Dallas, TX">Dallas, TX</option>
		    	<option value="Houston, TX">Houston, TX</option>
		    	<option value="Miami, FL">Miami, FL</option>
		    	<option value="Troy, MI">Troy, MI</option>
		    </select>
		    <select name="specialty">
		    	<option value="none">Service Line</option>
		    	<option value="Accounting, Audit and Assurance">Accounting, Audit and Assurance</option>
		    	<option value="Automotive Dealerships">Automotive Dealerships</option>
		    	<option value="Business Advisory">Business Advisory</option>
		    	<option value="Closely Held Businesses">Closely Held Businesses</option>
		    	<option value="Construction">Construction</option>
		    	<option value="Dental CPA">Dental CPA</option>
		    	<option value="Employee Benefit Plans">Employee Benefit Plans</option>
		    	<option value="Energy">Energy</option>
		    	<option value="Financial Institutions">Financial Institutions</option>
		    	<option value="Government & Non-Profit">Government & Non-Profit</option>
		    	<option value="Healthcare">Healthcare</option>
		    	<option value="Insurance">Insurance</option>
		    	<option value="International Companies">International Companies</option>
		    	<option value="IT Assurance Security">IT Assurance Security</option>
		    	<option value="iIT Assurance & Security">IT Assurance & Security</option>
		    	<option value="Mergers Acquisitions">Mergers Acquisitions</option>
		    	<option value="Payroll">Payroll</option>
		    	<option value="Public Companies">Public Companies</option>
		    	<option value="Real Estate">Real Estate</option>
		    	<option value="Retail Restaurant">Retail Restaurant</option>
		    	<option value="Service Providers">Service Providers</option>
		    	<option value="Small Businesses">Small Businesses</option>
		    	<option value="Strategic Advisory">Strategic Advisory</option>
		    	<option value="Tax Compliance and Planning">Tax Compliance and Planning</option>
		    	<option value="Technologies">Technologies</option>
		    	<option value="Valuation Litigation Support">Valuation Litigation Support</option>
		    	<option value="Wealth Management">Wealth Management</option>
		    	<option value="Wholesale and Distribution">Wholesale and Distribution</option>
		    </select>
		    <button type="submit" value="Submit">Apply</button>
	 	</form>
	</div>
</div>

<div class="jobs-feed feed-cotainer">
	<div class="job full-width even headings">
		<div class="job-position one-third">
			Position Title
		</div>
		<div class="job-area one-third">
			Service Line
		</div>
		<div class="job-location one-third">
			Location
		</div>
		<div style="clear: both"></div>
	</div>
	<?php
		query_posts( $args );
		if ( have_posts() ) : while ( have_posts() ) : the_post();
	?>

		<div class="job full-width <?php echo (++$j % 2 == 0) ? 'even' : 'odd'; ?>">
			<div class="job-position one-third">
				<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
			</div>
			<div class="job-area one-third">
				<?php the_field( 'job_specialty' ) ?>
			</div>
			<div class="job-location one-third">
				<?php the_field( 'job_location' ) ?>
			</div>
			<div style="clear: both"></div>
		</div>

	<?php endwhile; ?>
	<?php else : ?>
			<article>
				<h2>No Openings Were Found</h2>
			</article>
			<hr>
			<h2></h2>
			<?php 
				$argss = [ 
					'post_type' => 'jobs', 
					'order' => 'ASC', 
					'posts_per_page' => -1,
			    ];
				query_posts( $argss );
				if ( have_posts() ) : while ( have_posts() ) : the_post();
			?>
				<div class="job full-width <?php echo (++$j % 2 == 0) ? 'even' : 'odd'; ?>">
					<div class="job-position one-third">
						<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
					</div>
					<div class="job-area one-third">
						<?php the_field( 'job_specialty' ) ?>
					</div>
					<div class="job-location one-third">
						<?php the_field( 'job_location' ) ?>
					</div>
					<div style="clear: both"></div>
				</div>
				<?php endwhile; ?><?php endif; ?>
	<?php endif; ?>
	<div style="clear: both"></div>
</div>

