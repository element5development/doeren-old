<?php /*
TEMPLATE FOR DISPLAYING POSTS (VIEWPOINT) WITHIN THE ARCHIEVES OR CATEGORIES
*/ ?>

<article id="post-<?php the_ID(); ?>" class="post-feed viewpoint-archieves archieves-post full-width">

	<a href="<?php the_permalink(); ?>">
		<h3 class="entry-header">
			<?php 
				$title  = the_title('','',false);
				if(strlen($title) > 100) {
				    echo trim(substr($title, 0, 100)).'...';
				} else {
				    echo $title;
				}
			?>
		</h3>
	</a>
	<div class="post-feed-date"><?php the_time('m.d.y') ?></div> 
	<div class="archieves-contents">
		<div class="post-feed-excerpt">
			<?php $content = get_the_excerpt(); ?>
			<?php echo substr($content, 0, 250); ?>...
		</div> 
		<a class="read-more" href="<?php the_permalink(); ?>">Read Full Article <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-large-right.svg" /></a> 
	</div>

</article>
