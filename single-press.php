<?php /*
TEMPLATE FOR SINGLE CUSTOM POST TYPE "PRESS"
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<div class="back-to-parent max-width">
		<a class="back-page" href="/press/">Back to Press</a>
	</div>

	<section id="single-column-contents" class="max-width press-post">
		<h2><?php the_title(); ?></h2>
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</section>

</main>

<?php get_footer(); ?>