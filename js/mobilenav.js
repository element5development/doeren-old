/* Mobile Navigation Open and Close */

jQuery(document).ready(function () {
	jQuery("#menu-icon").click(function (e) {
		if (jQuery("#menu-icon").hasClass("mobile-icon-close")) {
			jQuery("#menu-icon").removeClass("mobile-icon-close");
			jQuery("#mobile-nav-expand").removeClass('mobile-open');
		} else {
			jQuery("#menu-icon").addClass("mobile-icon-close");
			jQuery("#mobile-nav-expand").addClass('mobile-open');
		}
	});
});

jQuery(document).ready(function () {
	$(".ginput_container_checkbox").parent().addClass('display-label');
	$(".ginput_container_radio").parent().addClass('display-label');
});