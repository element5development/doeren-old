var $j = jQuery.noConflict();

$j(function() {
  var offset = 6;
  $j("#viewpoint-images").load("/additional-viewpoint-images/?offset" + offset);
  $j("#load-more-viewpoint-images").click(function() {
    offset = offset + 6;
    $j("#viewpoint-images")
      .load("/additional-viewpoint-images/?offset=" + offset, function() {});
    return false;
  });
});