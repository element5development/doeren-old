var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		COOKIE POP UP
	\*----------------------------------------------------------------*/
	var visited = $.cookie('visited');
	if (visited == 'yes') {
		$('.cookie-use').css('display', 'none');
	} else {
		// do nothing
	}
	$.cookie('visited', 'yes', {
		expires: 1 // the number of days cookie  will be effective
	});
	$(".cookie-use button").click(function () {
		$('.cookie-use').addClass("is-hidden");
	});
});