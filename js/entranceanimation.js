/* Entrance Animations */

/*TEAM MEMBER FADE*/
jQuery(document).ready(function () {
	jQuery('.team-member').addClass("hidden").viewportChecker({
		classToAdd: 'visible animated fadeIn',
		offset: 100
	});
});
/*TIMELINE POSTS BOUNCE INTO SCREEN FROM RIGHT*/
jQuery(document).ready(function () {
	jQuery('.timeline-event.odd').addClass("hidden").viewportChecker({
		classToAdd: 'visible animated bounceInRight',
		offset: 100
	});
});
/*TIMELINE POSTS BOUNCE INTO SCREEN FROM LEFT*/
jQuery(document).ready(function () {
	jQuery('.timeline-event.even').addClass("hidden").viewportChecker({
		classToAdd: 'visible animated bounceInLeft',
		offset: 100
	});
});
/*EVENTS FADE*/
jQuery(document).ready(function () {
	jQuery('article.event.post-feed').addClass("hidden").viewportChecker({
		classToAdd: 'visible animated fadeIn',
		offset: 100
	});
});
/*RESOURCES FADE*/
jQuery(document).ready(function () {
	jQuery('article.resource.post-feed').addClass("hidden").viewportChecker({
		classToAdd: 'visible animated fadeIn',
		offset: 100
	});
});
/*PRESS FADE*/
jQuery(document).ready(function () {
	jQuery('article.press.post-feed').addClass("hidden").viewportChecker({
		classToAdd: 'visible animated fadeIn',
		offset: 100
	});
});

/*CHILD CATEGORIES*/
jQuery(document).ready(function () {
	$('.categories-widget li.has-child').on('click', function (e) {
		$(this).toggleClass('expanded');
	})
});