<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES & CATEGORY FOR CUSTOM POST TYPE "RESOURCES"
*/ ?>

<?php get_header(); ?>

<?php
	$service = $_GET["service"];
	$specialization = $_GET["specialization"];
	$format = $_GET["format"];

	//dental cpa both service and specialization
	if ( $specialization  == 'dental-cpa' ) {
		$service == 'dental-cpa';
		$specialization == 'none';
	}

	if ( $format == 'all' ) { unset($format); }
	if ( $service == 'none' ) { unset($service);  }
	if ( $specialization  == 'none' ) { unset($specialization );  }


	if ( $service == NULL and $specialization == NULL and $format == NULL) {
		/*NO VARIABLES PASSED*/
		$nothingpassed = "Nothing Passed";
	} elseif ( $specialization == NULL and $service == NULL ) {
	    /*FORMAT FILTER*/
		$args = [ 
			'post_type' => 'resources', 
			'order' => 'DESC', 
			'posts_per_page' => -1,
			'meta_key' => 'format',
			'meta_value' => $format,
	    ];
	} elseif ( $specialization == NULL ) {
	    /*NO SPECIALIZATION PASESD*/
		$args = [ 
			'post_type' => 'resources', 
			'order' => 'DESC', 
			'posts_per_page' => -1,
			'category_name' => $service,
	    ];
	} elseif ( $service == NULL  ) {
	    /*NO SERVICE PASSED */
		$args = [ 
			'post_type' => 'resources', 
			'order' => 'DESC', 
			'posts_per_page' => -1,
			'category_name' => $specialization,
	    ];
	} else {
		/*SERVICE & SPECALIZATION PASSED*/
		$args = [ 
			'post_type' => 'resources', 
			'order' => 'DESC', 
			'posts_per_page' => -1,
			'category_name' => $service.'+'.$specialization,
	    ];
	}
?>


<main class="full-width">

	<div class="page-header max-width">
		<?php echo do_shortcode('[rev_slider alias="resources-page"]'); ?>
	</div>

	<div id="page-contents-container" class="max-width archive-container">
		<div class="filter">
			<div class="filter-search resources">
				<div class="two-third">
					<form id="category-filter" method="get" action="/resources/">
					    <h2>Filter By:</h2>
					    <select name="service">
					    	<option value="none">Service</option>
					    	<option value="accounting-audit-assurance">Accounting, Audit and Assurance</option>
					    	<option value="business-advisory">Business Advisory</option>
					    	<option value="dental-cpa">Dental CPA</option>
					    	<option value="employee-benefit-plans">Employee Benefit Plans</option>
					    	<option value="insurance">Insurance</option>
					    	<option value="it-assurance-security">IT Assurance Security</option>
					    	<option value="mergers-acquisitions">Mergers Acquisitions</option>
					    	<option value="payroll">Payroll</option>
					    	<option value="strategic-advisory">Strategic Advisory</option>
					    	<option value="tax">Tax Compliance and Planning</option>
					    	<option value="valuation-litigation-support">Valuation Litigation Support</option>
					    	<option value="vwealth-management">Wealth Management</option>
					    </select>
					    <select name="specialization">
					    	<option value="none">Specialization</option>
					    	<option value="automotive-dealerships">Automotive Dealerships</option>
					    	<option value="closely-held-businesses">Closely Held Businesses</option>
					    	<option value="construction">Construction</option>
					    	<option value="dental-cpa">Dental CPA</option>
					    	<option value="energy">Energy</option>
					    	<option value="financial-institutions">Financial Institutions</option>
					    	<option value="government-non-profit">Government & Non-Profit</option>
					    	<option value="healthcare">Healthcare</option>
					    	<option value="international-companies">International Companies</option>
					    	<option value="public-companies">Public Companies</option>
					    	<option value="real-estate">Real Estate</option>
					    	<option value="retail-restaurant">Retail Restaurant</option>
					    	<option value="service-providers">Service Providers</option>
					    	<option value="small-businesses">Small Businesses</option>
					    	<option value="technologies">Technologies</option>
					    	<option value="wholesale-distribution">Wholesale & Distribution</option>
					    </select>
					    <button type="submit" value="Submit">Apply</button>
				 	</form>
				</div>
				<div class="one-third">
					<form role="search" method="get" id="searchform" class="searchform" action="/"> 
						<h2>Search:</h2>
						<input type="text" value="" name="s" placeholder="Search..." /> 
						<input type="hidden" name="post_type" value="resources" />
						<button type="submit" value="Search">Search</button>
					</form>
				</div>
				<div style="clear: both"></div>
			</div>
		</div>
		<?php if ( !isset($nothingpassed) ) { ?>
			<section id="single-sidebar-contents" class="left">
		<?php } else { ?>
			<section id="single-sidebar-contents" class="archive-category-feed left">
		<?php } ?>

			<div id="resources-nav" class="secondary-nav full-width">
				<nav>
					<?php wp_nav_menu( array( 'theme_location' => 'resourcess-nav' ) ); ?>
					<div style="clear: both"></div>
				</nav> 
			</div>

			<?php
				if ( !isset($nothingpassed) ) { query_posts( $args ); }
				if ( have_posts() ) : while ( have_posts() ) : the_post();
			?>	
				<?php get_template_part( 'template-parts/content', 'resource' ); ?>
			<?php endwhile; ?>
			<div style="clear: both"></div>
			<?php the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );
			else : ?>
				<article>
					<h2>No Resources Were Found</h2>
				</article>
				<hr>
				<h2></h2>
				<?php 
					$argss = [ 
						'post_type' => 'resources', 
						'order' => 'ASC', 
						'posts_per_page' => 5,
				    ];
					query_posts( $argss );
					if ( have_posts() ) : while ( have_posts() ) : the_post();
				?>
							<?php get_template_part( 'template-parts/content', 'resource' ); ?>
					<?php endwhile; ?><?php endif; ?>
			<?php endif; ?>

		</section>
		<aside id="single-sidebar" class="right widget-area-container resource-sidebar">
			<div class="grey-bg issues-bg">
				<h2>VIEWpoint Issues</h2>
				
				<?php
				$args = array(
				    'posts_per_page' => 4,
				    'post_type'      => 'resources',
				    'meta_key'		=> 'format',
					'meta_value'	=> 'issue'

				);

				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						get_template_part( 'template-parts/content', 'issue' );
					}
				} 
				?>
				<a class="primary-button" href="/resources/?format=issue">View All</a>

			</div>
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('single-right-sidebar')) : else : ?>
			<?php endif; ?>  
		</aside>
		<div style="clear: both"></div>
	</div>
	
</main>

<?php get_footer(); ?>