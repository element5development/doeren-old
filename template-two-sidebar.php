<?php /*
Template Name: Two Sidebar
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<?php get_template_part( 'template-parts/content', 'page-header' ); ?>

	<?php get_template_part( 'template-parts/content', 'sub-service' ); ?>

	<?php get_template_part( 'template-parts/content', 'secondary-nav' ); ?>

	<div id="page-contents-container" class="max-width">

		<aside id="double-left-sidebar" class="widget-area-container">


			<!--=====================-->
			<!--EVENTS QUERY LOGIC-->
			<!--=====================-->
			<?php //LOGIC
				// any from THIS category? 
					$page_slug = $post->post_name;
					switch_to_blog(1);
					$args = array(
						'posts_per_page' => 3,
						'post_type' => 'event',
						'event-category' => $page_slug,
					);
					$the_query = new WP_Query( $args );
					$post_count = $the_query->found_posts;
					restore_current_blog();
				// any from PRAENT category?
				// If no parent points to self
					// $parent_data = get_post($post->post_parent);
					// $parent_slug = $parent_data->post_name;
					// switch_to_blog(1);
					// $parent_args = array(
						// 'posts_per_page' => 3,
						// 'post_type' => 'event',
						// 'event-category' => $parent_slug,
					// );
					// $parent_query = new WP_Query( $parent_args );
					// $parent_post_count = $parent_query->found_posts;
					// restore_current_blog();
				// any from GRANDPARENT category? 
				// If no grandparent points to self
					// $gparent_data = get_post($parent_data->post_parent);
					// $gparent_slug = $gparent_data->post_name;
					// switch_to_blog(1);
					// $gparent_args = array(
						// 'posts_per_page' => 3,
						// 'post_type' => 'event',
						// 'event-category' => $gparent_slug,
					// );
					// $gparent_query = new WP_Query( $gparent_args );
					// $gparent_post_count = $gparent_query->found_posts;
					// restore_current_blog();
				// else GENERAL category 
			?>
			<?php switch_to_blog(1); //QUERY ?>
			<?php	if ( $post_count > 0 ) { ?>
				<div class="grey-bg">
					<h2>
						<a href="/events/event/">Upcoming Events</a>
					</h2>
					<?php while ( $the_query->have_posts() ) {
							$the_query->the_post();
							get_template_part( 'template-parts/content', 'event-side' );
						} ?>
				</div>
				<?php // } else if ( $parent_post_count > 0 && $parent_slug != 'specializations' ) { ?>
				<!-- <div class="grey-bg">
						<h2><a href="/events/event/">Upcoming Events</a></h2> -->
				<?php // while ( $parent_query->have_posts() ) {
							// $parent_query->the_post();
							// get_template_part( 'template-parts/content', 'event-side' );
						// } ?>
				<!-- </div> -->
				<?php // } else if ( $gparent_post_count > 0 && $gparent_slug != 'specializations' ) { ?>
				<!-- <div class="grey-bg">
						<h2><a href="/events/event/">Upcoming Events</a></h2> -->
				<?php // while ( $gparent_query->have_posts() ) {
							// $gparent_query->the_post();
							// get_template_part( 'template-parts/content', 'event-side' );
						// } ?>
				<!-- </div> -->
				<?php } else { ?>
				<!-- NOTHING -->
				<?php } ?>
				<?php wp_reset_query(); ?>
				<?php restore_current_blog(); ?>

				<!--=====================-->
				<!--VIEWPOINT QUERY LOGIC-->
				<!--=====================-->
				<?php //LOGIC
				// any from THIS category? 
					$page_slug = $post->post_name;
					switch_to_blog(1);
					$args = array(
						'category_name' => $page_slug,
						'posts_per_page' => 3,
						'post_type' => 'post',
						'orderby' => 'date',
				    'order' => 'DESC',
				    // Using the date_query to filter posts from the last 6 months
				    'date_query' => array(
				        array(
				            'after' => '6 month ago'
				        )
				    )
					);
					$the_query = new WP_Query( $args );
					$post_count = $the_query->found_posts;
					restore_current_blog();
				// any from PRAENT category?
				// If no parent points to self
					$parent_data = get_post($post->post_parent);
					$parent_slug = $parent_data->post_name;
					switch_to_blog(1);
					$parent_args = array(
						'category_name' => $parent_slug,
						'posts_per_page' => 3,
						'post_type' => 'post',
						'orderby' => 'date',
				    'order' => 'DESC',
				    // Using the date_query to filter posts from the last 6 months
				    'date_query' => array(
				        array(
				            'after' => '6 month ago'
				        )
				    )
					);
					$parent_query = new WP_Query( $parent_args );
					$parent_post_count = $parent_query->found_posts;
					restore_current_blog();
				// any from GRANDPARENT category? 
				// If no grandparent points to self
					$gparent_data = get_post($parent_data->post_parent);
					$gparent_slug = $gparent_data->post_name;
					switch_to_blog(1);
					$gparent_args = array(
						'category_name' => $gparent_slug,
						'posts_per_page' => 3,
						'post_type' => 'post',
						'orderby' => 'date',
				    'order' => 'DESC',
				    // Using the date_query to filter posts from the last 6 months
				    'date_query' => array(
				        array(
				            'after' => '6 month ago'
				        )
				    )
					);
					$gparent_query = new WP_Query( $gparent_args );
					$gparent_post_count = $gparent_query->found_posts;
					restore_current_blog();
				// else GENERAL category 
			?>
				<?php switch_to_blog(1); //QUERY ?>
				<?php	if ( $post_count > 0 ) { ?>
					<div class="grey-bg">
						<h2>
							<a href="/viewpoint/">Recent Articles</a>
						</h2>
						<?php while ( $the_query->have_posts() ) {
							$the_query->the_post();
							get_template_part( 'template-parts/content', 'viewpoint-side' );
						} ?>
					</div>
					<?php } else if ( $parent_post_count > 0 && $parent_slug != 'specializations' ) { ?>
					<div class="grey-bg">
						<h2>
							<a href="/viewpoint/">Recent Articles</a>
						</h2>
						<?php while ( $parent_query->have_posts() ) {
							$parent_query->the_post();
							get_template_part( 'template-parts/content', 'viewpoint-side' );
						} ?>
					</div>
					<?php } else if ( $gparent_post_count > 0 && $gparent_slug != 'specializations' ) { ?>
					<div class="grey-bg">
						<h2>
							<a href="/viewpoint/">Recent Articles</a>
						</h2>
						<?php while ( $gparent_query->have_posts() ) {
							$gparent_query->the_post();
							get_template_part( 'template-parts/content', 'viewpoint-side' );
						} ?>
					</div>
					<?php } else { ?>
					<?php 
						$args = array(
					    'posts_per_page' => 3,
					    'category_name' => 'general',
						);
						$general_query = new WP_Query( $args ); 
					?>
					<div class="grey-bg">
						<h2>
							<a href="/viewpoint/">Recent Articles</a>
						</h2>
						<?php while ( $general_query->have_posts() ) {
							$general_query->the_post();
							get_template_part( 'template-parts/content', 'viewpoint-side' );
						} ?>
					</div>
					<?php } ?>
					<?php wp_reset_query(); ?>
					<?php restore_current_blog(); ?>

					<!--=====================-->
					<!--CASE STUDY QUERY LOGIC-->
					<!--=====================-->
					<?php //LOGIC
					// any from THIS category? 
						$page_slug = $post->post_name;
						switch_to_blog(1);
						$args = array(
							'category_name' => $page_slug,
							'posts_per_page' => 3,
							'post_type' => 'case-study',
						);
						$the_query = new WP_Query( $args );
						$post_count = $the_query->found_posts;
						restore_current_blog();
					?>
					<?php switch_to_blog(1); //QUERY ?>
					<?php	if ( $post_count > 0 ) { ?>
						<div class="grey-bg">
							<h2>
								<a href="/case-study/">Case Studies</a>
							</h2>
							<?php while ( $the_query->have_posts() ) {
								$the_query->the_post();
								get_template_part( 'template-parts/content', 'case-study-side' );
							} ?>
						</div>
					<?php } ?>
					<?php wp_reset_query(); ?>
					<?php restore_current_blog(); ?>

					<!--SIDEBAR-->
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('two-sidebar-left')) : else : ?>
					<?php endif; ?>
		</aside>

		<section id="double-sidebar-contents">
			<div id="double-sidebar-page-contents">
				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section>

		<aside id="double-right-sidebar" class="widget-area-container">
			<!--===================-->
			<!--MEMBERS QUERY LOGIC-->
			<!--===================-->
			<?php 
			$posts = get_field('members');

			if( $posts ): ?>
				<div class="grey-bg member-widget">
					<h2>Lead Contacts</h2>
					<div class="my-flipster">
						<ul>
						<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
							<?php setup_postdata($post); ?>
							<?php get_template_part( 'template-parts/content', 'member-side' ); ?>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php else : ?>
				<?php echo do_shortcode('[gravityform id="7" title="false" description="false"]'); ?>
			<?php endif; ?>

			<!--=====================-->
			<!--RESOURCES QUERY LOGIC-->
			<!--=====================-->
			<?php //LOGIC
			// any from THIS category? 
				$page_slug = $post->post_name;
				switch_to_blog(1);
				$args = array(
					'category_name' => $page_slug,
					'posts_per_page' => 3,
					'post_type' => 'resources',
				);
				$the_query = new WP_Query( $args );
				$post_count = $the_query->found_posts;
				restore_current_blog();
			// any from PRAENT category?
			// If no parent points to self
				// $parent_data = get_post($post->post_parent);
				// $parent_slug = $parent_data->post_name;
				// switch_to_blog(1);
				// $parent_args = array(
				// 	'category_name' => $parent_slug,
				// 	'posts_per_page' => 3,
				//   'post_type' => 'resources',
				// );
				// $parent_query = new WP_Query( $parent_args );
				// $parent_post_count = $parent_query->found_posts;
				// restore_current_blog();
			// any from GRANDPARENT category? 
			// If no grandparent points to self
				// $gparent_data = get_post($parent_data->post_parent);
				// $gparent_slug = $gparent_data->post_name;
				// switch_to_blog(1);
				// $gparent_args = array(
				// 	'category_name' => $gparent_slug,
				// 	'posts_per_page' => 3,
				//   'post_type' => 'resources',
				// );
				// $gparent_query = new WP_Query( $gparent_args );
				// $gparent_post_count = $gparent_query->found_posts;
				// restore_current_blog();
			// else GENERAL category 
			?>
				<?php switch_to_blog(1); //QUERY ?>
				<?php	if ( $post_count > 0 ) { ?>
					<div class="grey-bg">
						<h2>
							<a href="/resources/">Resources</a>
						</h2>
						<?php while ( $the_query->have_posts() ) {
							$the_query->the_post();
							get_template_part( 'template-parts/content', 'resource-side' );
						} ?>
					</div>
					<?php // } else if ( $parent_post_count > 0 && $parent_slug != 'specializations' ) { ?>
					<!-- <div class="grey-bg">
						<h2>
							<a href="/resources/">Resources</a>
						</h2> -->
					<?php // while ( $parent_query->have_posts() ) {
							// $parent_query->the_post();
							// get_template_part( 'template-parts/content', 'resource-side' );
						// } ?>
					<!-- </div> -->
					<?php // } else if ( $gparent_post_count > 0 && $gparent_slug != 'specializations' ) { ?>
					<!-- <div class="grey-bg">
						<h2>
							<a href="/resources/">Resources</a>
						</h2> -->
					<?php // while ( $gparent_query->have_posts() ) {
							// $gparent_query->the_post();
							// get_template_part( 'template-parts/content', 'resource-side' );
						// } ?>
					<!-- </div> -->
					<?php } else { ?>
					<!-- NOTHING -->
					<?php } ?>
					<?php wp_reset_query(); ?>
					<?php restore_current_blog(); ?>

					<!--=====================-->
					<!--EVENTS QUERY LOGIC-->
					<!--=====================-->
					<?php //LOGIC
				// any from THIS category? 
					$page_slug = $post->post_name;
					switch_to_blog(1);
					$args = array(
						'posts_per_page' => 3,
						'post_type' => 'event',
						'event-category' => $page_slug,
					);
					$the_query = new WP_Query( $args );
					$post_count = $the_query->found_posts;
					restore_current_blog();
				// any from PRAENT category?
				// If no parent points to self
					$parent_data = get_post($post->post_parent);
					$parent_slug = $parent_data->post_name;
					switch_to_blog(1);
					$parent_args = array(
						'posts_per_page' => 3,
						'post_type' => 'event',
						'event-category' => $parent_slug,
					);
					$parent_query = new WP_Query( $parent_args );
					$parent_post_count = $parent_query->found_posts;
					restore_current_blog();
				// any from GRANDPARENT category? 
				// If no grandparent points to self
					$gparent_data = get_post($parent_data->post_parent);
					$gparent_slug = $gparent_data->post_name;
					switch_to_blog(1);
					$gparent_args = array(
						'posts_per_page' => 3,
						'post_type' => 'event',
						'event-category' => $gparent_slug,
					);
					$gparent_query = new WP_Query( $gparent_args );
					$gparent_post_count = $gparent_query->found_posts;
					restore_current_blog();
				// else GENERAL category 
			?>
					<?php switch_to_blog(1); //QUERY ?>
					<?php	if ( $post_count > 0 ) { ?>
						<div class="grey-bg mobile-widget">
							<h2>
								<a href="/events/event/">Upcoming Events</a>
							</h2>
							<?php while ( $the_query->have_posts() ) {
							$the_query->the_post();
							get_template_part( 'template-parts/content', 'event-side' );
						} ?>
						</div>
						<?php } else if ( $parent_post_count > 0 && $parent_slug != 'specializations' ) { ?>
						<div class="grey-bg mobile-widget">
							<h2>
								<a href="/events/event/">Upcoming Events</a>
							</h2>
							<?php while ( $parent_query->have_posts() ) {
							$parent_query->the_post();
							get_template_part( 'template-parts/content', 'event-side' );
						} ?>
						</div>
						<?php } else if ( $gparent_post_count > 0 && $gparent_slug != 'specializations' ) { ?>
						<div class="grey-bg mobile-widget">
							<h2>
								<a href="/events/event/">Upcoming Events</a>
							</h2>
							<?php while ( $gparent_query->have_posts() ) {
							$gparent_query->the_post();
							get_template_part( 'template-parts/content', 'event-side' );
						} ?>
						</div>
						<?php } else { ?>
						<!-- NOTHING -->
						<?php } ?>
						<?php wp_reset_query(); ?>
						<?php restore_current_blog(); ?>

						<!--=====================-->
						<!--VIEWPOINT QUERY LOGIC-->
						<!--=====================-->
						<?php //LOGIC
				// any from THIS category? 
					$page_slug = $post->post_name;
					switch_to_blog(1);
					$args = array(
						'category_name' => $page_slug,
						'posts_per_page' => 3,
						'post_type' => 'post',
						'orderby' => 'date',
				    'order' => 'DESC',
				    // Using the date_query to filter posts from the last 6 months
				    'date_query' => array(
				        array(
				            'after' => '6 month ago'
				        )
				    )
					);
					$the_query = new WP_Query( $args );
					$post_count = $the_query->found_posts;
					restore_current_blog();
				// any from PRAENT category?
				// If no parent points to self
					$parent_data = get_post($post->post_parent);
					$parent_slug = $parent_data->post_name;
					switch_to_blog(1);
					$parent_args = array(
						'category_name' => $parent_slug,
						'posts_per_page' => 3,
						'post_type' => 'post',
						'orderby' => 'date',
				    'order' => 'DESC',
				    // Using the date_query to filter posts from the last 6 months
				    'date_query' => array(
				        array(
				            'after' => '6 month ago'
				        )
				    )
					);
					$parent_query = new WP_Query( $parent_args );
					$parent_post_count = $parent_query->found_posts;
					restore_current_blog();
				// any from GRANDPARENT category? 
				// If no grandparent points to self
					$gparent_data = get_post($parent_data->post_parent);
					$gparent_slug = $gparent_data->post_name;
					switch_to_blog(1);
					$gparent_args = array(
						'category_name' => $gparent_slug,
						'posts_per_page' => 3,
						'post_type' => 'post',
						'orderby' => 'date',
				    'order' => 'DESC',
				    // Using the date_query to filter posts from the last 6 months
				    'date_query' => array(
				        array(
				            'after' => '6 month ago'
				        )
				    )
					);
					$gparent_query = new WP_Query( $gparent_args );
					$gparent_post_count = $gparent_query->found_posts;
					restore_current_blog();
				// else GENERAL category 
			?>
						<?php switch_to_blog(1); //QUERY ?>
						<?php	if ( $post_count > 0 ) { ?>
							<div class="grey-bg mobile-widget">
								<h2>
									<a href="/viewpoint/">Recent Articles</a>
								</h2>
								<?php while ( $the_query->have_posts() ) {
							$the_query->the_post();
							get_template_part( 'template-parts/content', 'viewpoint-side' );
						} ?>
							</div>
							<?php } else if ( $parent_post_count > 0 && $parent_slug != 'specializations' ) { ?>
							<div class="grey-bg mobile-widget">
								<h2>
									<a href="/viewpoint/">Recent Articles</a>
								</h2>
								<?php while ( $parent_query->have_posts() ) {
							$parent_query->the_post();
							get_template_part( 'template-parts/content', 'viewpoint-side' );
						} ?>
							</div>
							<?php } else if ( $gparent_post_count > 0 && $gparent_slug != 'specializations' ) { ?>
							<div class="grey-bg mobile-widget">
								<h2>
									<a href="/viewpoint/">Recent Articles</a>
								</h2>
								<?php while ( $gparent_query->have_posts() ) {
							$gparent_query->the_post();
							get_template_part( 'template-parts/content', 'viewpoint-side' );
						} ?>
							</div>
							<?php } else { ?>
							<?php 
						$args = array(
					    'posts_per_page' => 3,
					    'category_name' => 'general',
						);
						$general_query = new WP_Query( $args ); 
					?>
							<div class="grey-bg mobile-widget">
								<h2>
									<a href="/viewpoint/">Recent Articles</a>
								</h2>
								<?php while ( $general_query->have_posts() ) {
							$general_query->the_post();
							get_template_part( 'template-parts/content', 'viewpoint-side' );
						} ?>
							</div>
							<?php } ?>
							<?php wp_reset_query(); ?>
							<?php restore_current_blog(); ?>

							<!--RIGHT SIDEBAR-->
							<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('two-sidebar-right')) : else : ?>
							<?php endif; ?>
							<!--SIDEBAR-->
							<div class="mobile-widget">
								<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('two-sidebar-left')) : else : ?>
								<?php endif; ?>
							</div>
		</aside>

		<div style="clear: both"></div>

	</div>
</main>

<?php get_footer(); ?>