<?php /*
Template Name: Right Sidebar
*/ ?>

<?php get_header(); ?>

<main class="full-width full-page-container">

	<?php get_template_part( 'template-parts/content', 'page-header' ); ?>

	<?php get_template_part( 'template-parts/content', 'secondary-nav' ); ?>

	<div id="page-contents-container" class="max-width">
		<section id="single-sidebar-contents" class="left">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; ?>

			<!--AFFILIATATIONS FEED-->
			<?php if( is_page('39') ) { ?>	
				<?php get_template_part( 'template-parts/content', 'affiliates' ); ?>
			<?php } ?>

			<!--OUR VALUE PROPOSITION-->
			<?php if( is_page('54') ) { ?>	
				<?php get_template_part( 'template-parts/content', 'value-proposition' ); ?>
			<?php } ?>
			
		</section>
		<aside id="single-sidebar" class="right widget-area-container">
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('single-right-sidebar')) : else : ?>
				<p><strong>Widget Ready</strong></p>  
			<?php endif; ?>  
		</aside>
		<div style="clear: both"></div>
	</div>
</main>

<?php get_footer(); ?>